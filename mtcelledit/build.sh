#!/bin/sh
set -e

for pkgdir in       \
    libmtkit        \
    libmtcelledit   \
    libmtpixy       \
    libmtcedui      \
    libmtqex        \
    mtcelledit
do
    (
        cd "$(dirname "$0")/${pkgdir}"
        echo "========================================================================"
        echo "$pkgdir : $(pwd)"
        echo "========================================================================"
        makepkg
        pacman -U ./${pkgdir}-*.pkg.*
    )
done
