#!/bin/sh
set -e

for pkgdir in       \
    libmtkit        \
    libmtcelledit   \
    libmtpixy       \
    libmtcedui      \
    libmtqex        \
    mtcelledit
do
    (
        cd "$(dirname "$0")/${pkgdir}"
        echo "========================================================================"
        echo "$pkgdir : $(pwd)"
        echo "========================================================================"
        sudo -u build aur build
        sudo pacman -U /var/cache/pacman/aur/${pkgdir}-*.pkg.*
    )
done
