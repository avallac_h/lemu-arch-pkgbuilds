#!/bin/sh

echo "==> Downloading files..."
for i in "$(dirname "$0")"/*.link ; do

    var1=$(grep -v -e '^[[:space:]]' -e '^[[:space:]]*[#;]' "$i" | awk -F'::' '{ print $1; }')
    var2=$(grep -v -e '^[[:space:]]' -e '^[[:space:]]*[#;]' "$i" | awk -F'::' '{ print $2; }')

    if [ -z "$var1" ] ; then
        continue
    elif [ -z "$var2" ] ; then
        echo "  -> Downloading \"$var1\" -> \"$(basename "$var1")\"..."
        curl -C - -RLO "$var1"
    else
        echo "  -> Downloading \"$var2\" -> \"$var1\"..."
        curl -C - -RLo "$var1" "$var2"
    fi

done 

echo "==> Verifying files..."
for i in md5 sha1 sha256 sha512 ; do
    find "$(dirname "$0")" -iname "*.$i" -printf '  -> %f\n' -exec "${i}sum" --quiet -c '{}' \;
done
